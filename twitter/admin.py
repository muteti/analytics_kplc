from django.contrib import admin

from .models import Tweet, TwitterConfig


class TweetAdmin(admin.ModelAdmin):
    list_display = ['tweet_id_no', 'location']


class TwitterConfigAdmin(admin.ModelAdmin):
    list_display = ['agent',]


admin.site.register(Tweet, TweetAdmin)
admin.site.register(TwitterConfig, TwitterConfigAdmin)