import tweepy

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView, FormView

# from authentication.models import Account
from .models import Tweet, TwitterConfig
from .forms import SearchForm, TwitterConfigForm
from .tasks import get_keyword
from analytic.forms import EventForm


@login_required(login_url='/auth/login/')
def twitter(request):
    user = request.user
    tweets = Tweet.objects.all().order_by('created_at').reverse()
    page = request.GET.get('page', 1)

    paginator = Paginator(tweets, 25)
    try:
        tweet = paginator.page(page)
    except PageNotAnInteger:
        tweet = paginator.page(1)
    except EmptyPage:
        tweet = paginator.page(paginator.num_pages)

    error = False
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        else:
            texts = Tweet.objects.filter(text__icontains=q)
            return render(request, 'search_results.html', {'texts': texts, 'query': q})
    if user.user_type == 'A':
        return render(request, 'twitter/twitter.html', {'tweet': tweet, 'error': error})
    else:
        return HttpResponse("You do not have the rights")


def bad_search(request):
    message = 'You searched for: %r' % request.GET['q']
    return HttpResponse(message)


class TweetsListView(ListView):
    template_name = 'twitter/twitter_list.html'
    model = Tweet


class GenerateKeyWordView(FormView):
    template_name = 'twitter/twitter-test.html'
    form_class = SearchForm
    context_object_name = 'names'  # Default: object_list
    paginate_by = 10
    # success_url = '/twitter/'

    def get_context_data(self, **kwargs):
        context = super(GenerateKeyWordView, self).get_context_data(**kwargs)
        agent = self.request.user
        conf = TwitterConfig.objects.get(agent=agent)
        consumer_key = conf.consumer_key
        consumer_secret = conf.consumer_secret
        access_token = conf.access_token
        access_secret = conf.access_secret
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_secret)
        api = tweepy.API(auth)
        woeid = 23424863
        trends1 = api.trends_place(woeid)
        data = trends1[0]
        trends = data['trends']
        names = [trend['name'] for trend in trends]
        context['trendsName'] = names
        return context


    def form_valid(self, form):
        keyword = form.cleaned_data.get('keyword')
        agent = self.request.user.id
        # tweet_category_id = form.cleaned_data.get('tweet_category_id')
        # print(tweet_category_id)
        get_keyword.delay(keyword, agent)
        messages.success(self.request, 'Tracking your search terms')
        return redirect('twitter')


# @login_required(login_url='/auth/login/')
def twitter_settings(request):
    if request.method == "POST":
        form = TwitterConfigForm(request.POST)
        if form.is_valid():
            conf = form.save(commit=False)
            conf.agent = request.user
            conf.save()
            return redirect('twitter:twitter')
    else:
        form = TwitterConfigForm()

    return render(request, 'twitter/twitter-settings.html', {'form': form})


# @login_required(login_url='/auth/login/')
def post_tweet(request, tweet_id=None):
    # tweet_id = None
    if request.method == 'GET':
        tweet_id = request.GET['tw_id']
        tweet = Tweet.objects.get(id=int(tweet_id))
        event_form = EventForm(initial={'report':tweet.text})
    else:
        event_form = EventForm(request.POST, request.FILES)
        if event_form.is_valid():
            tweet = event_form.save(commit=False)
            tweet.date_reported = timezone.now()
            tweet.agent = request.user
            tweet.save()
            return HttpResponseRedirect(reverse('twitter:twitter'))
    return render(request, 'events/report.html', {'event_form': event_form})


