from crispy_forms import layout, bootstrap
from crispy_forms.helper import FormHelper
from django import forms

from .models import Tweet, TwitterConfig


# Search keyword/Hashtag form using the twitter API
class SearchForm(forms.ModelForm):
    keyword = forms.CharField(max_length=200)

    class Meta:
        model = Tweet
        fields = ('tweet_category',)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "POST"

        self.helper.layout = layout.Layout(
            layout.Fieldset(
                '',
                layout.Field("tweet_category", css_class='form-control'),
                layout.Field("keyword", placeholder='Search using hashtags or keywords', css_class='form-control'),
                bootstrap.FormActions(
                    layout.Submit("submit", ("Search"), css_class="btn btn-primary"),
                ),
            ),
        )


# search tweets in the db
class TweetSearchForm(forms.Form):
    search_term = forms.CharField(max_length=200)


# Form to configure tweet authentication details of user
class TwitterConfigForm(forms.ModelForm):
    class Meta:
        model = TwitterConfig
        fields = ('consumer_key','consumer_secret','access_token', 'access_secret',)

    def __init__(self, *args, **kwargs):
        super(TwitterConfigForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "POST"

        self.helper.layout = layout.Layout(
            layout.Fieldset(
                '',
                layout.Field("consumer_key", placeholder='Enter consumer key', css_class='form-control'),
                layout.Field("consumer_secret", placeholder='Enter consumer secret', css_class='form-control'),
                layout.Field("access_token", placeholder='Enter access token', css_class='form-control'),
                layout.Field("access_secret", placeholder='Enter access secret', css_class='form-control'),
                bootstrap.FormActions(
                    layout.Submit("submit", ("Save"), css_class="btn btn-primary"),
                ),
            ),
        )
