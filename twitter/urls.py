from django.contrib import admin
from django.conf.urls import url

from . import views

admin.autodiscover()

urlpatterns = [
    url(r'^post-tweet/$', views.post_tweet, name='post-tweet'),
    url(r'^twitter-settings/$', views.twitter_settings, name='twitter-settings'),
    url(r'^search/$', views.GenerateKeyWordView.as_view(), name='search-form'),
    url(r'^twitter/$', views.twitter, name='twitter'),
]