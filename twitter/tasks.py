import tweepy
import json

from celery.schedules import crontab
from celery.task import periodic_task
from celery.utils.log import get_task_logger
from dateutil import parser
from celery import shared_task, task
from django.http import HttpRequest

from .models import Tweet, TwitterConfig

logger = get_task_logger(__name__)


@shared_task
def get_keyword(keyword, agent):
    conf = TwitterConfig.objects.get(agent=agent)
    consumer_key = conf.consumer_key
    consumer_secret = conf.consumer_secret
    access_token = conf.access_token
    access_secret = conf.access_secret
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    listener = StreamListener(api=tweepy.API(wait_on_rate_limit=True))
    streamer = tweepy.Stream(auth=auth, listener=listener)
    print("Tracking: " + keyword)
    streamer.filter(track=[str(keyword)], locations=[33.8935689697, -4.67677, 41.8550830926, 5.506])
    return '{} Will be looked up on twitter live stream!'.format(keyword)


def store_data(location, tweet_id_no, created_at, text):
    # tweet_category = get_keyword(tweet_category_id)
    # print(tweet_category_id)
    tweet = Tweet.objects.create(
        location=location,
        tweet_id_no=tweet_id_no,
        created_at=created_at,
        text=text
        # tweet_category=tweet_category
    )
    tweet.save()


class StreamListener(tweepy.StreamListener):

    def on_connect(self):
        print("You are now connected to the streaming API.")

    def on_error(self, status_code):
        while True:
            try:
                print('An Error has occured: ' + repr(status_code))
            except:
                continue

    def on_data(self, data):
        try:
            datajson = json.loads(data)
            text = datajson['text']
            location = datajson['user']['location']
            tweet_id_no = datajson['id']
            created_at = parser.parse(datajson['created_at'])
            print("Tweet collected at " + str(location))

            store_data(location, tweet_id_no, created_at, text,)

        except Exception as e:
            print(e)
