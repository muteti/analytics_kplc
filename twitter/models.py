from django.db import models
from django.utils import timezone
from analytic.models import Category


# Create your models here.
from analytics import settings


class Tweet(models.Model):
    text = models.TextField(max_length=140)
    tweet_id_no = models.CharField(max_length=200)
    location = models.CharField(max_length=200, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    tweet_category = models.ForeignKey(Category, related_name='tweet_events', on_delete=models.CASCADE, null=True,
                                       blank=True)

    def __str__(self):
        return self.tweet_id_no


class TwitterConfig(models.Model):
    agent = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    consumer_key = models.CharField(max_length=200)
    consumer_secret = models.CharField(max_length=200)
    access_token = models.CharField(max_length=200)
    access_secret = models.CharField(max_length=200)

    def __str__(self):
        return str(self.agent_id)