from channels import route
from channels import include
from analytic import consumers
# from sms import sms_consumers


event_routing = [
    route('websocket.connect', consumers.ws_connect),
    route('websocket.receive', consumers.ws_receive),
    route('websocket.disconnect', consumers.ws_disconnect),
]


# sms_routing = [
#     route('websocket.connect', sms_consumers.sms_connect),
#     route('websocket.receive', sms_consumers.sms_receive),
#     route('websocket.disconnect', sms_consumers.sms_disconnect),
# ]


channel_routing = [
    include(event_routing, path=r"^/events/$"),
    # include(sms_routing, path=r"^/sms/sms/$"),
]