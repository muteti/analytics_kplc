from django.conf.urls import include, url
from django.views.generic import TemplateView

from analytic import views, admin
from analytic.views import  TableDataView
from django.conf.urls import url

from djgeojson.views import GeoJSONLayerView

from .models import Event
from . import views




urlpatterns = [
    url(r'^$', views.map, name='map'),
    url(r'^teables/$', TableDataView.as_view(), name='tables'),
    url(r'^leaflet/$', views.leaflet, name='leaflet'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^events/$', views.data, name='data'),
    url(r'^unverified_events/$', views.unverified_data, name='unverified_data'),
    url(r'^report/$', views.new_event, name='report'),
    url(r'^post-event/$', views.update_event, name='post-event'),
    # url(r'^appdata/$', views.appdata, name='appdata'),
    # url(r'^help/$', views.help, name='help'),
    url(r'^data.geojson/$',
        GeoJSONLayerView.as_view(model=Event, properties=('report', 'name', 'date_verified', 'category',)),
        name='events_data'),
    url(r'^(?P<category_slug>[-\w]+)/$', views.category_detail, name='event_category'),
    url(r'^event/(?P<id>[0-9A-Fa-f-]+)/$', views.event_detail, name='event_detail'),
]
