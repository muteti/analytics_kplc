from channels import Group


def ws_connect(message):
    message.reply_channel.send({"accept": True})
    Group('event').add(message.reply_channel)


def ws_receive(message):
    Group('event').add(message.reply_channel)


def ws_disconnect(message):
    Group('event').discard(message.reply_channel)



