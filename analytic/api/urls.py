from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from .views import (EventList, create_event, create_report, EventDetailsView, api_root,
                    CategoryList)


urlpatterns = [
    url(r'^$', api_root, name='events-api-root'),
    url(r'^events-list/?$', EventList.as_view(), name=EventList.name),
    url(r'^categories-list/?$', CategoryList.as_view(), name=CategoryList.name),
    url(r'^event-create/$', create_event, name='event-create'),
    url(r'^report-event/$', create_report, name='report-event'),
    url(r'^event-detail/(?P<pk>[0-9]+)/$',EventDetailsView.as_view(), name=EventDetailsView.name),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)