from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from analytic.models import Event, Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name', 'id')


class EventSerializer(GeoFeatureModelSerializer):
    agent = serializers.ReadOnlyField(source='agent.username')
    # event_image = serializers.ImageField(max_length=None, use_url=True, read_only=True)

    class Meta:
        model = Event
        geo_field = 'geom'
        fields = ('id','agent','name', 'report', 'category', 'address', 'status', 'event_image')




