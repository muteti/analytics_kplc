from django.http import Http404
from django.urls import reverse

from rest_framework import permissions, status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.decorators import api_view, parser_classes, permission_classes
from rest_framework.response import Response
from rest_framework.reverse import reverse

from analytic.models import Event, Category

from analytic.api.serializers import EventSerializer, CategorySerializer


class EventList(APIView):
    name = 'events-list'
    # permission = permissions.IsAuthenticatedOrReadOnly

    def get(self, request, format=None):
        # agent = request.user
        events = Event.objects.all()
        serializer = EventSerializer(events, many=True)
        # data = {
        #     "data": serializer.data,
        #     "error": False,
        #     "message": 'Event List'
        # }
        return Response(serializer.data)


class CategoryList(APIView):
    """
    List of all event categories
    """
    name = 'categories-list'
    permission = permissions.IsAuthenticatedOrReadOnly

    def get(self, request, format=None):
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        data = {
            "data": serializer.data,
            "error": False,
            "message": 'Category List'
        }
        return Response(data)


# @api_view(["POST"])
# # @parser_classes((MultiPartParser, FormParser,))
# def create_AppData(request):
#     serializer = AppDataSerializer(data=request.data)
#     if serializer.is_valid():
#         serializer.save(agent=request.user)
#         return Response({"error": False,
#                          "message": str(request.data)})
#     else:
#         data = {
#           "error": True,
#           "message": str(serializer.errors),
#         }
#         print(serializer.errors, data)
#         return Response(data)


@api_view(["POST"])
@parser_classes((MultiPartParser, FormParser,))
def create_event(request):
    serializer = EventSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save(agent=request.user)
        return Response({"error": False,
                         "message": serializer.data})
    else:
        data = {
          "error": True,
          "message": str(serializer.errors),
        }
        print(serializer.errors, data)
        return Response(data)


@api_view(["POST"])
@permission_classes((permissions.AllowAny, ))
@parser_classes((MultiPartParser, FormParser,))
def create_report(request):
    serializer = EventSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save(status='unverified')
        return Response({"error": False,
                         "message": serializer.data})
    else:
        data = {
          "error": True,
          "message": str(serializer.errors),
        }
        print(serializer.errors, data)
        return Response(data)



class EventDetailsView(APIView):
    name = 'event-detail'
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (permissions.IsAuthenticated)

    def get_object(self, pk):
        try:
            return Event.objects.get(pk=pk)
        except Event.DoesNotExist:
                raise Http404

    def get(self, request, pk, format=None):
        event = self.get_object(pk)
        event = EventSerializer(event)
        return Response({"data":event.data, "error": True})

    def put(self, request, pk, format=None):
        event = self.get_object(pk=pk)
        serializer = EventSerializer(event, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request, pk, format=None):
        event = self.get_object(pk)
        event.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# class TweetDataList(APIView):
#     name = 'twitter-list'
#     permission = permissions.IsAuthenticatedOrReadOnly
#
#     def get(self, request, format=None):
#         tweets = TweetData.objects.all().order_by('-id')[:3]
#         serializer = TweetDataSerializer(tweets, many=True)
#         # data = {
#         #     "data": serializer.data,
#         #     "error": False,
#         #     "message": 'Category List'
#         # }
#
#         print(serializer.data)
#         return Response(serializer.data)


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'events-list': reverse(EventList.name, request=request, format=format),
        'event-create': reverse('event-create', request=request, format=format),
        'category-list': reverse(CategoryList.name, request=request, format=format),
        'access-token': reverse('token', request=request, format=format),
        'login': reverse('api-login', request=request, format=format),
        'sms-create': reverse('text-create', request=request, format=format),
        'sms-list': reverse('sms-list', request=request, format=format),
        'report-event': reverse('report-event', request=request, format=format),
    })



