from django.contrib.gis import admin

from .models import Category, Event


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name',]
    prepopulated_fields = {'slug':('name',)}


class EventAdmin(admin.OSMGeoAdmin):
    list_display = ['report', 'date_reported', 'category', 'event_image']
    prepopulated_fields = {'slug': ('name',)}


class TweetDataAdmin(admin.ModelAdmin):
    list_display = ['tweet_id', 'location']


admin.site.register(Category, CategoryAdmin)
admin.site.register(Event, EventAdmin)