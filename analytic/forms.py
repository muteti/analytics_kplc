from django import forms
from django.core.urlresolvers import reverse

from crispy_forms import layout, bootstrap
from leaflet.forms.widgets import LeafletWidget
from crispy_forms.helper import FormHelper

from .models import Event


# Report Event form
class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ('name', 'report', 'category', 'status', 'geom', 'event_image')
        widgets = {'geom': LeafletWidget()}

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse("map")
        self.helper.form_method = "POST"

        self.helper.layout = layout.Layout(
            layout.Fieldset(
                '',
                layout.Field("name", placeholder='Enter event name', css_class='form-control'),
                layout.Field("report", placeholder='Enter event report'),
                layout.Field("category"),
                layout.Field("status"),
                layout.Field("geom"),
                layout.Field("event_image"),
                bootstrap.FormActions(
                    layout.Submit("submit", ("Submit"), css_class="btn btn-primary"),
                ),
            ),
        )