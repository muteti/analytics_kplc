import uuid
from django.db import models
from django.contrib.gis.db import models
from django.utils import timezone
# Create your models here.
from django.urls import reverse

STATUS_CHOICES = (
    ('verified', 'Verified'),
    ('unverified', 'Unverified'),)

class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)

    class Meta:
        ordering = 'name',
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('event_category',
                       args=[self.slug])

class EventManager(models.Manager):
    def get_queryset(self):
        #exclude out events with null name fields
        return super(EventManager, self).get_queryset().filter(status='verified', name__gt='', name__isnull=False)


class UnverifiedEventManager(models.Manager):
    def get_queryset(self):
        #fetch events with null name fields
        return super(UnverifiedEventManager, self).get_queryset().filter(status='unverified')

class Event(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # agent = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    slug = models.SlugField(max_length=200, null=True, blank=True)
    date_reported = models.DateTimeField(auto_now_add=True)
    report = models.TextField()
    event_image = models.ImageField(upload_to='event_images', null=True, blank=True)
    category = models.ForeignKey(Category, related_name='events' ,on_delete=models.CASCADE, null=True, blank=True)
    address = models.TextField(max_length=200, null=True, blank=True)
    geom = models.PointField(null=True)
    status = models.CharField(max_length=10,
                              choices=STATUS_CHOICES,
                              default='unverified')
    objects = models.GeoManager()
    verified = EventManager()
    unverified = UnverifiedEventManager()

    def __str__(self):
        return self.report

    def get_absolute_url(self):
        return reverse('event_detail',
                       args=[self.id])
