from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core import serializers
from channels import Group

from .models import Event


@receiver(post_save, sender=Event)
def event_broadcaster(sender, instance, created, **kwargs):
    if created:
        event_data = serializers.serialize('json', [instance,])
        print(event_data)
        Group('event').send({'text': event_data})
