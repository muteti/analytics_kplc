from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView

from authentication.models import Account
from .forms import EventForm
from .models import Event, Category

# from sms.models import Text
from twitter.models import Tweet


@login_required(login_url='/auth/login/')
def map(request):
    tweets = Tweet.objects.all().order_by('-id')[:3]
    return render(request, 'events/map2.html', {'tweets': tweets})


class TableDataView(TemplateView):
    template_name = "tables.html"


def leaflet(request):
    return render(request, 'leaflet.html', {})


@login_required(login_url='/auth/login/')
def dashboard(request):
    user = request.user
    users = Account.objects.all()
    user_count = users.count()
    events = Event.verified.order_by('-date_reported')[:5]
    events_count = Event.objects.all().count()
    tweet = Tweet.objects.all().count()
    latest_tweets = Tweet.objects.order_by('-created_at')[:5]
    text = Text.objects.all().count()

    if user.user_type == 'A':
        return render(request,
                      'events/dashboard.html',
                      {'events': events,
                       'users': users,
                       'user_count': user_count,
                       'events_count': events_count,
                       'tweet_count': tweet,
                       'text_count': text
                       })
    else:
        return HttpResponse("You do not have the access rights!!")


@login_required(login_url='/auth/login/')
def data(request):
    user = request.user
    events = Event.verified.all().order_by('date_reported').reverse()
    page = request.GET.get('page', 1)

    paginator = Paginator(events, 8)
    try:
        event = paginator.page(page)
    except PageNotAnInteger:
        event = paginator.page(1)
    except EmptyPage:
        event = paginator.page(paginator.num_pages)
    if user.user_type == 'A':
        return render(request,
                      'events/data.html',
                      {'event': event})
    else:
        return HttpResponse("You do not have the access rights")


@login_required(login_url='/auth/login/')
def unverified_data(request):
    user = request.user
    events = Event.unverified.all().order_by('date_reported').reverse()
    page = request.GET.get('page', 1)

    paginator = Paginator(events, 8)
    try:
        event = paginator.page(page)
    except PageNotAnInteger:
        event = paginator.page(1)
    except EmptyPage:
        event = paginator.page(paginator.num_pages)
    if user.user_type == 'A':
        return render(request,
                      'events/unverified_data.html',
                      {'event': event})
    else:
        return HttpResponse("You do not have the access rights")


@login_required(login_url='/login/')
def new_event(request):
    user = request.user
    if request.method == 'POST':
        event_form = EventForm(request.POST, request.FILES)
        if event_form.is_valid():
            event = event_form.save(commit=False)
            event.date_reported = timezone.now()
            event.agent = request.user
            event.save()
            return HttpResponseRedirect(reverse('analytic:map'))
    else:
        event_form = EventForm()
    return render(request, 'events/report.html', {'event_form': event_form})


@login_required(login_url='/auth/login/')
def category_detail(request, category_slug=None):
    user = request.user
    categories = Category.objects.all()
    category = get_object_or_404(Category, slug=category_slug)
    event_category = Event.objects.filter(category=category)
    if user.user_type == 'A':
        return render(request, 'events/event_category.html', {'category': category,
                                                       'categories': categories,
                                                       'event_category': event_category})
    else:
        return HttpResponse("You do not have rights!!")


@login_required(login_url='/auth/login/')
def event_detail(request, id):
    event = get_object_or_404(Event,
                                id=id)

    return render(request,
                  'events/event_detail.html',
                  {'event': event})


@login_required
def update_event(request, event_id=None):
    event_id = request.GET['event_id']
    event = get_object_or_404(Event, id=event_id)
    event_form = EventForm(request.POST or None, request.FILES or None, instance=event)
    if request.method == 'POST':
        if event_form.is_valid():
            event_form.save()
            return redirect('analytic:map')
    return render(request, 'events/report.html', {'event_form': event_form})



