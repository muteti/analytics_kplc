from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import Account
from .forms import AccountChangeForm, AccountCreationForm


class UserAdmin(BaseUserAdmin):
    form = AccountChangeForm
    add_form = AccountCreationForm
    list_display = ('email','first_name', 'is_admin', 'user_type',)
    list_filter = ('is_admin',)
    fieldsets = (
        ('Profile', {'fields': ('email', 'username','first_name', 'last_name', 'user_type', 'password',)}),
        ('Permissions', {'fields': ('is_staff','is_admin',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email','password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

admin.site.register(Account, UserAdmin)

