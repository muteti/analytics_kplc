from django.conf.urls import url, include

from .views import user_login, agent_reports, users_list, logout

urlpatterns = [
    url(r'^login/$', user_login, name='login'),
    url(r'^agent-reports/(?P<username>[-\w]+)/$', agent_reports, name='agent_reports'),
    url(r'^users/$', users_list, name='users'),
    url(r'^logout/$', logout, name='logout'),
]
