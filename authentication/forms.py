from crispy_forms import layout, bootstrap
from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.urlresolvers import reverse

from .models import Account


class AccountCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Account
        fields = 'email','username','first_name','last_name','user_type',

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords Didn't Match")
        return password2

    def save(self, commit=True):
        user = super(AccountCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class AccountChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Account
        fields = ('email', 'password', 'is_admin', 'user_type',)

    def clean_password(self):
        return self.initial["password"]


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse("analytic:map")
        self.helper.form_method = "POST"

        self.helper.layout = layout.Layout(
            layout.Fieldset(
                '',
                layout.Field("username", placeholder='Username',css_class="form-control"),
                layout.Field("password", placeholder='Password', css_class="form-control"),

            ),
        )


class AccountProfileForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ('username', 'email', 'first_name', 'last_name',)
