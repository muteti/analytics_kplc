from django.conf.urls import url
from rest_framework.authtoken.views import obtain_auth_token

from authentication.api.views import login

urlpatterns = [
    url(r'^login/$', login, name='api-login'),
    url(r'^token/$', obtain_auth_token, name='token'),
]