from django.contrib.auth import authenticate

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.authtoken.models import Token


@api_view(["POST"])
def login(request):
    """
    Login via API endpoint 
    """
    username = request.data.get("username")
    password = request.data.get("password")

    user = authenticate(username=username, password=password)
    if not user:
        return Response({"error": "True", "message": "No user with provided credentials"})

    token, _ = Token.objects.get_or_create(user=user)
    return Response({"error": "False", "message": "Successfully Authenticated!", "token": token.key})
