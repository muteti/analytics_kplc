from django.shortcuts import render

from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as usalama_logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.db import connections

from .models import Account
from .forms import LoginForm
from analytic.models import Event


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'],
                                password=cd['password'])
            if user is not None:
                login(request, user)
                if user.is_active and user.user_type == 'A':
                    return redirect('analytic:map')
                else:
                    return render(request, 'events/map.html')
            else:
                return HttpResponse('You do not have an account')
        else:
            return HttpResponse('Invalid credentials')
    else:
        form = LoginForm()
    return render(request, 'auth/login.html', {'form': form})


@login_required(login_url='/auth/login/')
def agent_reports(request, username):
    agent = request.user
    user = get_object_or_404(Account, username=username)
    all_events = Event.objects.filter(agent=user).order_by('date_reported').reverse()
    event_count = all_events.count()
    page = request.GET.get('page', 1)
    paginator = Paginator(all_events,10)

    try:
        event = paginator.page(page)
    except PageNotAnInteger:
        event = paginator.page(1)
    except EmptyPage:
        event = paginator.page(paginator.num_pages)
    if agent.user_type == 'A':
        return render(request,'auth/profile.html',{'user': user, 'event': event, 'event_count': event_count})
    else:
        return HttpResponse("You do not have the rights!!")


@login_required(login_url='/auth/login/')
def users_list(request):
    agent = request.user
    users = Account.objects.all()
    if agent.user_type == 'A':
        return render(request, 'auth/users.html', {'users': users})
    else:
        return HttpResponse("You do not have the rights!!")


def logout(request):
    usalama_logout(request)
    return redirect('login')









